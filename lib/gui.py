import tkinter as tk
from tkinter import messagebox, filedialog
from tkinter import ttk
import os


class MainApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)


        ####Variables needed for backend
        self.input_dir = tk.StringVar(self, None)
        self.output_dir = tk.StringVar(self, None)
        self.sample_sheet = tk.StringVar(self, None)

        self.geometry("400x400")
        self.wm_title("BCL2FASTQ")
        self.title("BCL2FASTQ")

        self.show_input_frame()


    def show_input_frame(self):
        input_frame = tk.LabelFrame(self, text="BCL --> FASTQ", width=40)
        input_frame.pack()

        inframe = tk.LabelFrame(input_frame, text="BCL Klasörü Seç")
        inframe.pack()
        ttk.Button(inframe, text='Klasörü Seç', command=self.select_input_folder, textvariable=self.input_dir, width=12).pack()

        out_entry_fr = tk.LabelFrame(input_frame, text="Çıktı klasörü")
        out_entry_fr.pack()
        self.out_entry = tk.Entry(out_entry_fr, textvariable=self.output_dir)
        self.out_entry.pack()

        sample_sheet_fr = tk.LabelFrame(input_frame, text="Sample Sheet dosyası seçin")
        sample_sheet_fr.pack()

        ttk.Button(sample_sheet_fr, text='SampleSheet', command=self.select_sample_sheet, width=12,
                  textvariable=self.sample_sheet).pack()


        ttk.Button(self, text='ONAYLA', command=self.execute_bcl2fastq, width=10).pack()

    def select_input_folder(self):
        self.input_dir.set(filedialog.askdirectory())
        if self.input_dir.get() not in ("", None):
            outdir = os.path.join(self.input_dir.get(), "OUTPUT")
            self.out_entry.insert(0, outdir)
            self.output_dir.set(outdir)


    def select_sample_sheet(self):
        self.sample_sheet.set(filedialog.askopenfilename())

    def execute_bcl2fastq(self):
        from lib.utils import call_subprocess
        output_dir = self.output_dir.get()
        input_dir = self.input_dir.get()
        sample_sheet = self.sample_sheet.get()

        os.chdir(input_dir)
        if output_dir in (None, ""):
            output_dir = os.path.join(input_dir, "OUTPUT")
            if not os.path.exists(output_dir):
                os.mkdir(output_dir)
        if sample_sheet in (None, ""):
            sample_sheet = os.path.join(input_dir, "SampleSheet.csv")

        try:
            call_subprocess("bcl2fastq",
                        ["--output-dir", output_dir, "--sample-sheet", sample_sheet])
            messagebox.showinfo("Dönüşüm tamam", "FASTQ dosyaları oluşturuldu")
        except Exception as exc:
            messagebox.showerror("ERROR", "Hata ile karşaşıldı!!")
            messagebox.showerror("ERROR", str(exc))
            raise exc
        finally:
            self.quit()



