#!/usr/bin/env bash
ProjectDir=$(dirname -- "$( readlink -f -- $0;)")

#pip install -r "$ProjectDir/requirements.txt"

#AppVersion=$(cat $ProjectDir/_version.py | tr "=" " " | awk '{print $NF}')
IconPath="$ProjectDir/data/dna.ico"
ExecutePath="$ProjectDir/main.py"
DesktopLauncher=$HOME/Desktop/B2F.desktop

sudo chmod +x $ExecutePath

echo "[Desktop Entry]
Version=1.0
Name=B2F
Exec=$ExecutePath
Icon=$IconPath
Type=Application
Terminal=false
StartupWMClass=B2f" > $DesktopLauncher
gio set $DesktopLauncher metadata::trusted true
sudo chmod +x $DesktopLauncher

desktop-file-validate $DesktopLauncher
