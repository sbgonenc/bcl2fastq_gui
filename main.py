#!/usr/bin/env python3
from lib.gui import MainApp

if __name__ == '__main__':
    app = MainApp(className="B2F")
    app.mainloop()

